#include "ConnectedComponentSegmentation.h"
#include <map>
#include <pcl/point_cloud.h>

using namespace std;

namespace cc
{

	template <template <typename> class C, typename T>
	ConnectedComponentSegmentation<C,T>::ConnectedComponentSegmentation()
	{
		init();
	}

	template <template <typename> class C, typename T>
	ConnectedComponentSegmentation<C,T>::ConnectedComponentSegmentation(ConnectedComponentParams params)
	{
		this->params = params;
		init();
	}

	template <template <typename> class C, typename T>
	ConnectedComponentSegmentation<C,T>::~ConnectedComponentSegmentation()
	{
	}

	template <template <typename> class C, typename T>
	bool ConnectedComponentSegmentation<C,T>::Segment(const C<T>* ptCloud, vector<int>& labels, vector<Region>& regions)
	{
		if(!ptCloud)
		{
			cout << "Invalid data" << endl;
			return false;
		}

		unsigned int size_cloud = ptCloud->size();

		if(!size_cloud)
		{
			cout << "Point cloud is empty or unorganized" << endl;
			return false;
		}

		if(!allocate(size_cloud))
		{
			cout << "Not enough memory, stack overflow" << endl;
			return false;
		}

		cloud_ = ptCloud;

		firstPass();
		secondPass();

		map<int, vector<int> >  indices;
		for(int i = 0; i < cloud_->size(); i++){
			if(labels_[i] == -1)
				continue;
			indices[labels_[i]].push_back(i);
		}

		for( map<int, vector<int> >::iterator it = indices.begin(); it != indices.end(); ++it )
		{
			if( (*it).second.size() > params.minSizeRegion && (*it).second.size() < params.maxSizeRegion)
			{
				regions.push_back(cc::Region((*it).first, (*it).second));
			}
		}

		labels = labels_;
		return true;
	}

	template <template <typename> class C, typename T>
	bool ConnectedComponentSegmentation<C,T>::firstPass()
	{
		/*
		O	B
		A	X(actual)
		*/
		typename C<T>::const_iterator itCurrent	= cloud_->begin();		//X
		typename C<T>::const_iterator itTop		= cloud_->begin();		//B
		typename C<T>::const_iterator itLeft	= cloud_->begin();		//A

		int width = params.width;

		int i = 0;
		int last_label = 0;
		for(; itCurrent < cloud_->end(); i++, ++itCurrent){
			//convert linear to 2d coordinates
			int x, y;
			x = i % width;
			y = i / width;
			bool not_top=y>0;
			bool not_left=x>0;

			/*
			if((*itCurrent).z == 0) {
			background_[i] = 0;
			continue;
			}
			*/

			if(not_top)itTop=itCurrent-width;
			if(not_left)itLeft=itCurrent-1;

			float diffAX = INT_MAX;
			float diffBX = INT_MAX;
			float diffAB = INT_MAX;
			if(not_top)
				if(background_[i-width])
					diffBX = comparator_->compare((*itCurrent), (*itTop));
			if(not_left)
				if(background_[i-1])
					diffAX = comparator_->compare((*itCurrent), (*itLeft)); // calculate AX diff

			//both background, create a new label
			if(diffBX == INT_MAX && diffAX == INT_MAX){
				labels_[i] = last_label++;
				continue;
			}

			if(diffBX == INT_MAX){ // If top is not valid
				if(diffAX < params.comparisonThreshold){ //compare with left
					labels_[i] = labels_[i-1];
				}else{ //assign new label if not near
					labels_[i] = last_label++;
				}
				continue;
			}
			if(diffAX == INT_MAX){ // If left is not valid
				if(diffBX < params.comparisonThreshold){ // compare with top
					labels_[i] = labels_[i-width];
				}else{ //assign new label if not near
					labels_[i] = last_label++;
				}
				continue;
			}

			if(diffAX < params.comparisonThreshold){
				labels_[i] = labels_[i-1];
                if(diffBX < params.comparisonThreshold)
                    hierarchy_[labels_[i-1]] = hierarchy_[labels_[i-width]];
			}
            else if(diffBX < params.comparisonThreshold){
				labels_[i] = labels_[i-width];
                if(diffAX < params.comparisonThreshold)
                    hierarchy_[labels_[i-width]] = hierarchy_[labels_[i-1]];
			}
            else{
			    labels_[i] = last_label++;
            }

            /*
            diffAB = comparator_->compare((*itTop), (*itLeft));
            if(diffAB < params.comparisonThreshold){
                if(labels_[i-1] <= labels_[i-width])
                    hierarchy_[labels_[i-width]] = hierarchy_[labels_[i-1]];
                else
                    hierarchy_[labels_[i-1]] = hierarchy_[labels_[i-width]];
            }
             */
		}
		return true;
	}

	template <template <typename> class C, typename T>
	bool ConnectedComponentSegmentation<C,T>::secondPass()
	{
		for(int i = 0; i < cloud_->size(); i++){
			int index = labels_[i];

			if(index == -1)
				continue;

			while(index != hierarchy_[index]){
				index = hierarchy_[index];
			}
			labels_[i] = index;
		}
		return true;
	}

	template <template <typename> class C, typename T>
	bool ConnectedComponentSegmentation<C,T>::allocate(unsigned int size)
	{
		try{
			hierarchy_.resize(size);
			labels_.resize(size);
			background_.resize(size);
		}
		catch(...){
			return false;
		}

		//fill hierarchy data
		vector<int>::iterator it = hierarchy_.begin();
		for(int counter = 0; it != hierarchy_.end(); ++it, ++counter)
			(*it)=counter;

		//fill labels array with -1 
		fill(labels_.begin(), labels_.end(), -1);

		//set all pixels to foreground, no background masking is implemented
		fill(background_.begin(), background_.end(), 1);

		return true;
	}

	template <template <typename> class C, typename T>
	ConnectedComponentParams& ConnectedComponentSegmentation<C,T>::GetParams()
	{
		return this->params;
	}

	template <template <typename> class C, typename T>
	void ConnectedComponentSegmentation<C,T>::SetComparator(shared_ptr<Comparator<T> > comparator)
	{
		comparator_ = comparator;
	}

	template <template <typename> class C, typename T>
	void ConnectedComponentSegmentation<C,T>::init()
	{
		shared_ptr<Comparator<T> > ptr(new EuclideanComparator<T>());
		SetComparator(ptr);
	}

} // namespace cc

template class cc::ConnectedComponentSegmentation< pcl::PointCloud, pcl::PointXYZRGB >;
template class cc::ConnectedComponentSegmentation< cc::VectorWrapper, float >;