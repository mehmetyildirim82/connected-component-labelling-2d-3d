#ifndef WRAPPERS_H
#define WRAPPERS_H

namespace cc{

	/*!
     * @brief This is a sample wrapper for a vector type container. Segmentation is compatible only with iterating objects.
     * @author Mehmet Yildirim
     * \ingroup Connected Components
     * @{
     */
	template <typename T>
	struct VectorWrapper
	{
		typedef typename std::vector<T>::iterator iterator;
		typedef typename std::vector<T>::const_iterator const_iterator;

		iterator begin () { return (data.begin ()); }
		iterator end ()   { return (data.end ()); }
		const_iterator begin () const { return (data.begin ()); }
		const_iterator end () const  { return (data.end ()); }

		///Any wrapper must return the size of the underlying data.
		size_t size() const { return data.size(); }

		///Does NOT have to be public.
		std::vector<T> data;
	};
	/*! @}*/

} // namespace cc

#endif //WRAPPERS_H