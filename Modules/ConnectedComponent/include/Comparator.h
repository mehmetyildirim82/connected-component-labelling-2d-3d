#ifndef COMPARATOR_H
#define COMPARATOR_H

#include <pcl/point_types.h>
#include <cmath>

namespace cc
{
    /*!
     * @brief Interface for customising comparison methods for connected component segmentation.
     * @author Mehmet Yildirim
     * \ingroup Connected Components
     * @{
     */
    template <typename T>
    class Comparator
    {
    public:
        /*!
         * \brief This function must be overridden to define a comparison method.
         * \param pt1 is the first point to compare.
         * \param pt2 is the second point to compare.
         * \return Returns the difference between two points.
         */
        virtual float compare(const T& pt1, const T& pt2)=0;

        ///Shared pointer to the base object.
        typedef std::shared_ptr<Comparator<T> > Ptr;
    };
    /*! @}*/

    /*!
     * @brief Defines an example of a customized comparison method.
     * @author Mehmet Yildirim
     * \ingroup Connected Components
     * @{
     */
	template <typename T>
	class EuclideanComparator : public Comparator<T>
	{
	public:
        ///Not intended for direct usage, overridden in specialized templates.
		virtual float compare(const T& pt1, const T& pt2){};
	};
    /*! @}*/

    /*!
     * @brief Specialized template for EculideanComparator. Defines a comparison method only for pcl::PointXYZRGB.
     * @author Mehmet Yildirim
     * \ingroup Connected Components
     * @{
     */
	//TODO: Document this
	template<>
	class EuclideanComparator<pcl::PointXYZRGB> : public Comparator<pcl::PointXYZRGB>
	{
	public:
        ///Euclidean distance between two pcl points.
		float compare(const pcl::PointXYZRGB& pt1, const pcl::PointXYZRGB& pt2)
		{
            float dist;
			dist = sqrt( pow(pt1.x-pt2.x, 2.f) + pow(pt1.y-pt2.y, 2.f) + pow(pt1.z-pt2.z, 2.f) );

            if( (!pt1.x && !pt1.y && !pt1.z) || (!pt2.x && !pt2.y && !pt2.z))
                dist = 1000.f;

			return dist;
		}
	};
    /*! @}*/

    /*!
     * @brief Specialized template for floating point comparison.
     * @author Mehmet Yildirim
     * \ingroup Connected Components
     * @{
     */
	template<>
	class EuclideanComparator<float> : public Comparator<float>
	{
	public:
        ///Very simple example for floating point comparison. Implemented for demonstration.
		float compare(const float& pt1, const float& pt2)
		{
			float dist = std::abs(pt1-pt2);
			return dist;
		}
	};
    /*! @}*/

} // namespace cc

#endif //COMPARATOR_H