#ifndef CONNECTED_COMPONENT_SEGMENTATION_H
#define CONNECTED_COMPONENT_SEGMENTATION_H

#include <memory>
#include <vector>

#include "ConnectedComponentParams.h"
#include "Comparator.h"
#include "Wrappers.h"

namespace cc
{
    ///Type to hold labels after segmentation.
    typedef std::vector<int> Labels;
    ///Regions holds its label at first and indices at second part of the pair.
    //TODO:Consider converting this to a struct/class
    typedef std::pair< int, std::vector<int> > Region;
    ///Array of regions, just for convenience.
    typedef std::vector<Region> Regions;

    /*!
     * @brief Implementation of a generic connected components segmentation algorithm for organized data sets.
     * @author Mehmet Yildirim
     * \ingroup Connected Components
     * @{
     */
    template <template <typename> class C, typename T>
    class ConnectedComponentSegmentation
    {
    public:
        ConnectedComponentSegmentation();
        ~ConnectedComponentSegmentation();

        ///Parameters can be implicitly converted to ConnectedComponentSegmentation objects.
        ConnectedComponentSegmentation(ConnectedComponentParams params);

        /*!
         * \brief
         * \param [in] ptCloud Pointer to an organized input data set, with known size. The given object must be able to iterate over the data.
         * Compatible comparison methods must be provided.
         * \param [out] labels holds the index of the corresponding region for all elements in the input data.
         * \param [out] indices are regions in data, within the given size range.
         * \return Returns true if
         */
        bool Segment(const C<T>* ptCloud, std::vector<int>& labels, std::vector<Region>& indices);

        /*!
         * \brief Used to change the comparison method.
         * \param comparator is a pointer to a comparator object.
         */
        void SetComparator(std::shared_ptr<Comparator<T> > comparator);

        ///Reference to parameters, do whatever you want with parameters.
        ConnectedComponentParams& GetParams();

    private:
        ///Initializes member components, i.e. sets the default comparator.
        void init();

        /*!
         *  O	B
         *  A	X(actual)
         *
         *  Compares X with A and B.
         *  Gets A or B's label, if the distance is close enough.
         *  Compares A and B, if they are close each other too, writes regions of A and B into list of regions 'to be merged'.
         */
        bool firstPass();

        /// Merges the regions marked as linked together in the first pass.
        bool secondPass();

        ///Allocate stack memory for internal data
        ///Returns false if the stack is full.
        bool allocate(unsigned int size);

        ///Data that is given by the user.
        const C<T>* cloud_;
        ///This is actually not implemented. Intended as a background mask given by the user.
        ///Real usage of this would increase the efficiency.
        std::vector<int> background_;

        std::vector<int> labels_;

        std::vector<int> hierarchy_;

        ///Comparison method.
        std::shared_ptr<Comparator<T> > comparator_;

        ///Parameters
        ConnectedComponentParams params;

    public:
        ///Copy construction and Assignment operation are not allowed.
        ConnectedComponentSegmentation(const ConnectedComponentSegmentation& copy);
        ConnectedComponentSegmentation& operator=(const ConnectedComponentSegmentation& copy);
    };
    /*! @}*/

} // namespace cc

#endif