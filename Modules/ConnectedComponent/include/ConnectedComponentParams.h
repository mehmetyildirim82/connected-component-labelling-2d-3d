#ifndef CONNECTED_COMPONENT_PARAMS_H
#define CONNECTED_COMPONENT_PARAMS_H

namespace cc 
{

	struct ConnectedComponentParams
	{
		///Regions contain less than this amount of points is out of interest. Too small.
		int minSizeRegion;

		///Regions contain more than this amount of points is out of interest. Too large.
		int maxSizeRegion;

		///Two points those similarities rated below this value are grouped together
		///Example: euclidean distance between two < comparisonThreshold
		float comparisonThreshold;

		///Width of the organized data set
		int width;

        ///Height of the organized data set
		int height;

		ConnectedComponentParams() :
		//Default values
		minSizeRegion(15000),
			maxSizeRegion(40000),
			comparisonThreshold(0.1f),
			width(640), 
			height(480)
		{}
	};

} // namespace cc

#endif