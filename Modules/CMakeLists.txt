cmake_minimum_required(VERSION 2.8.11)

set(MODULES_PATH "${CMAKE_CURRENT_SOURCE_DIR}")
add_subdirectory(ConnectedComponent)