// concon.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <algorithm>
#include <memory>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "ConnectedComponentSegmentation.h"

using namespace std;
using namespace cc;

void TestPCL(const char* filename, const char* result);

int main(int argc, char* argv[])
{
	cout << argc << endl;
	if(argc != 2){
		cout << "Please pass path to a pcd file" << endl;
		return 0;
	}

	TestPCL(argv[1],"result.pcd");
	return 0;
}

void TestPCL(const char* filename, const char* result)
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
	pcl::io::loadPCDFile<pcl::PointXYZRGB>(filename, *cloud);

	cc::Regions reg;
	cc::Labels labels;

	ConnectedComponentParams params;
    params.comparisonThreshold = 0.3;
    params.minSizeRegion = 25000;
	params.maxSizeRegion = 100000;
	ConnectedComponentSegmentation<pcl::PointCloud, pcl::PointXYZRGB > cc = params;
	cc.Segment(const_cast<pcl::PointCloud<pcl::PointXYZRGB>*>(cloud.get()), labels, reg);

	//regions
	cout << "Found Regions:" << endl;
	for(int i = 0;i <  reg.size(); i++)
	{
		cout << "reg id: " << reg[i].first << "\nreg size: " << reg[i].second.size() << endl;
	}

	if(reg.empty()) {
		cout << "cant find any regions." << endl;
		return;
	}

	//Points do not belong to the first region are painted blank.
	for(int i = 0; i < cloud->width; i++){
		int val = labels[i];

		//if(val == reg[0].first)
		//	continue;

		cloud->points[i].r = labels[i]*20;
		cloud->points[i].g = labels[i]*20;
		cloud->points[i].b = labels[i]*20;
	}

	pcl::io::savePCDFileBinary(result, *cloud);
	cout << " Result has been written as '" << result << "'\n";
	cout << " Selected regions preserve their point colors, rest becomes black\n";
}