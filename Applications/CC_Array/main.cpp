#include <iostream>
#include <algorithm>
#include <memory>
#include <map>
#include <limits>

#include "ConnectedComponentSegmentation.h"

using namespace std;
using namespace cc;

void TestArray();

int main(int argc, char* argv[])
{
	TestArray();
	return 0;
}

void TestArray()
{
	//Sample data set with known size
	float data[16] = {	1.5,    1.5,    0,      0,
						1.5,    1.5,    1.5,    0,
						0,      1.05,   1.2,    0,
						0,      0,      1.2,    1.2,
					};

	//We use a wrapper around a vector
	VectorWrapper<float> v_data;
	//copy array data into vector
	v_data.data.assign(begin(data), end(data));

	//Set parameters
	ConnectedComponentParams params;
	params.comparisonThreshold = 0.4;
	params.minSizeRegion = 0;
	params.maxSizeRegion = 100000;
	params.width = 4;
	params.height = 4;
	//Implicit conversion from parameters to segmentation possible
	ConnectedComponentSegmentation<VectorWrapper, float> cc = params;

	//data preparation for output of the algorithm
	cc::Labels labels;
	cc::Regions regions;

	//We set the comparator for floating values. No worries. Very easy to implement. Check file Comparator.h.
	shared_ptr<Comparator<float> > comp(new EuclideanComparator<float>());
	cc.SetComparator(comp);

	//Does the rest of the work.
	cc.Segment(&v_data, labels, regions);

	//labels after segmentation
	for(int i = 0; i < 16; i+=4)
	{
		cout << labels[i] << " ";
		cout << labels[i+1] << " ";
		cout << labels[i+2] << " ";
		cout << labels[i+3] << " ";
		cout << endl;
	}
}